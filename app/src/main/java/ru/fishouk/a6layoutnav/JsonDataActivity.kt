package ru.fishouk.a6layoutnav


import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.AsyncTask
import android.util.Log
import android.widget.*
import org.json.JSONObject
import java.net.URL
import org.json.JSONArray
import org.json.JSONException
import java.util.*


class JsonDataActivity : AppCompatActivity() {
   companion object {
        private var mListViewJsonData: ListView? = null
        private val LOG_TAG = "my_log"
        private var mDataFinishArr = ArrayList<HashMap<String, Any>>()
        private var mAdapter: SimpleAdapter? = null
        private val ID = "id"
        private val NAME = "name"
   }

   override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_json_data)

        ParseTask().execute()
        mListViewJsonData = findViewById(R.id.listViewJsonData) as ListView
        mAdapter = SimpleAdapter(this, mDataFinishArr,
               android.R.layout.simple_list_item_2, arrayOf(ID, NAME), intArrayOf(android.R.id.text1, android.R.id.text2))
        mListViewJsonData!!.setAdapter(mAdapter)
   }

    private class ParseTask : AsyncTask<Void, Void, String>() {

        override fun doInBackground(vararg params: Void): String{

            val mResultJson: String = URL("http://ad.api.zigzag-mobile.com/public/getRubrics").readText()

            return mResultJson
        }

        override fun onPostExecute(mStrJson: String) {
            super.onPostExecute(mStrJson)
            Log.d(LOG_TAG, mStrJson)
            val mDataJasonObj: JSONObject

            try {
                mDataJasonObj = JSONObject(mStrJson)
                val mList: JSONArray = mDataJasonObj.getJSONObject("data").getJSONArray("list")

                for (i in 0..mList.length() - 1) {
                    val mListItem = mList.getJSONObject(i)

                    val hm = HashMap<String, Any>()
                    hm.put(ID, mListItem.getString("id"))
                    hm.put(NAME, mListItem.getString("name"))
                    mDataFinishArr.add(hm)
                }
                mAdapter!!.notifyDataSetChanged()
            }
            catch (e: JSONException) {
                e.printStackTrace()
            }
        }
   }
}
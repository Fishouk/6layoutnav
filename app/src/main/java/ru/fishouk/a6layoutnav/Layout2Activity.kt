package ru.fishouk.a6layoutnav

import android.support.v7.app.AppCompatActivity
import android.os.Bundle

class Layout2Activity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_layout2)
    }
}

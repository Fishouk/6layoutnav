package ru.fishouk.a6layoutnav

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun onClickButtonGoToSecondLayout (view: View) {
        val intent = Intent(this, Layout2Activity::class.java)
        startActivity(intent)
    }
    fun onClickButtonGoToTherdLayout (view: View) {
        val intent = Intent(this, Layout3::class.java)
        startActivity(intent)
    }
    fun onClickButtonGoToJsonLayout (view: View) {
        val intent = Intent(this, JsonDataActivity::class.java)
        startActivity(intent)
    }
}
